﻿#include "UPLib\\UP_System.h"
#include "soft_reset.h"

// #define DEBUG

int AD1 = 0;        //底部前方红外光电
int AD2 = 0;        //底部右侧红外光电
int AD3 = 0;        //底部后方红外光电
int AD4 = 0;        //底部左侧红外光电

int AD5 = 0;        //前红外测距传感器
int AD6 = 0;        //右红外测距传感器
int AD7 = 0;        //后红外测距传感器
int AD8 = 0;        //左红外测距传感器

int AD9 = 0;        //左前防掉台红外光电
int AD10 = 0;       //右前防掉台红外光电
int AD11 = 0;       //右后防掉台红外光电
int AD12 = 0;       //左后防掉台红外光电

int AD15 = 0;       //倾角传感器

int nStage = 0; //检测在台上还是在台下
int nEdge = 0;  //边缘
int nEnemy = 0; //敌人
int nFence = 0; //台下状态

int Qian = 0;       //前有无敌人
int You = 0;        //有有无敌人
int Hou = 0;        //后有无敌人
int Zuo = 0;        //左有无敌人
int Summ = 0;   //前后左右几个检测到

int left = 0;
int right = 0;

int na = 0;     //倾斜计时
int nb = 0;         //推箱子计时
int nc = 8;         //旋转计时
int nd = 0;         //前搁浅计时
int ne = 8;         //后搁浅计时

int QX = 0;         //倾斜

void zhong() {  //上台默认动作
	int adjusted_value = 150;
	UP_CDS_SetAngle(5, 384 - adjusted_value, 800);
	UP_CDS_SetAngle(6, 640 + adjusted_value, 800);
	UP_CDS_SetAngle(7, 640 + adjusted_value, 800);
	UP_CDS_SetAngle(8, 384 - adjusted_value, 800);
}

void qding() { //收前爪
	int adjusted_value = 100;
	UP_CDS_SetAngle(5, 1000 - adjusted_value, 800);
	UP_CDS_SetAngle(6, 24 + adjusted_value, 800);
	UP_delay_ms(10);
	UP_CDS_SetAngle(5, 1000 - adjusted_value, 800);
	UP_CDS_SetAngle(6, 24 + adjusted_value, 800);
}

void hding() { //收后爪
	int adjusted_value = 100;
	UP_CDS_SetAngle(7, 24 + adjusted_value, 800);
	UP_CDS_SetAngle(8, 1000 - adjusted_value, 800);
	UP_delay_ms(10);
	UP_CDS_SetAngle(7, 24 + adjusted_value, 800);
	UP_CDS_SetAngle(8, 1000 - adjusted_value, 800);
}

void chanzi() { //上台后铲子状态
	int adjusted_value = 50;
	UP_CDS_SetAngle(5, 780 - adjusted_value, 800);
	UP_CDS_SetAngle(6, 244 + adjusted_value, 800);
	UP_CDS_SetAngle(7, 244 + adjusted_value, 800);
	UP_CDS_SetAngle(8, 780 - adjusted_value, 800);
}

void move(int forward, int turn) {
	left = forward;
	right = turn;
	if (left > 1023) {
		left = 1023;
	}
	if (left < -1023) {
		left = -1023;
	}
	if (right > 1023) {
		right = 1023;
	}
	if (right < -1023) {
		right = -1023;
	}
	UP_CDS_SetSpeed(1, left);
	UP_CDS_SetSpeed(2, -right);
	UP_CDS_SetSpeed(3, left);
	UP_CDS_SetSpeed(4, -right);
}


void qianshangtai() { //前上台
	move(0, 0); //停下来，防止前一状态是转弯改变上台方向
	UP_delay_ms(100);
	zhong();//四支架抬起为支擂台做准备
	UP_delay_ms(400);
	move(500, 500); //对准擂台
	UP_delay_ms(700);
	qding();//前爪支地
	UP_delay_ms(800);
	UP_CDS_SetAngle(5, 384, 512);
	UP_CDS_SetAngle(6, 640, 512);
	UP_delay_ms(500);
	hding();//支后腿
	UP_delay_ms(800);
	zhong();
	//chanzi();//收后爪到铲子状态
	UP_delay_ms(1000);
	move(0, 0); //
	UP_delay_ms(500);
}

void houshangtai() { //后上台
	move(0, 0); //停下来，防止前一状态是转弯改变上台方向
	UP_delay_ms(100);
	zhong();//四支架抬起为支擂台做准备
	UP_delay_ms(400);
	move(-500, -500); //对准擂台
	UP_delay_ms(600);
	hding();//前爪支地
	UP_delay_ms(900);
	UP_CDS_SetAngle(7, 640, 512);
	UP_CDS_SetAngle(8, 384, 512);
	UP_delay_ms(500);
	qding();//支后腿
	UP_delay_ms(800);
	zhong();
	//chanzi();//收后爪到铲子状态
	UP_delay_ms(1000);
	move(0, 0); //
	UP_delay_ms(500);
}


#define STAGE_UNDERSTAGE 0
#define STAGE_ONSTAGE 1
#define STAGE_ONSTAGE_RIGHT 3
#define STAGE_ONSTAGE_LEFT 4
unsigned char Stage() { //检测是否在台上
	AD15 = UP_ADC_GetValue(15);
	Qian = (!UP_ADC_GetIO(1)) | UP_ADC_GetIO(5);
	You  = (!UP_ADC_GetIO(2)) | UP_ADC_GetIO(6);
	Hou  = (!UP_ADC_GetIO(3)) | UP_ADC_GetIO(7);
	Zuo  = (!UP_ADC_GetIO(4)) | UP_ADC_GetIO(8);
	Summ = Qian + Hou + Zuo + You;
	if ((AD15 > 2050) && (AD15 < 2350)) {
		if (Summ >= 2) {
			return 0;  //在台下
		} else {
			return 1;   //在台上
		}
	} else if (AD15 < 2050) {
		return 3;   //卡在擂台左侧在地面右侧在擂台
	} else {
		return 4;   //卡在擂台右侧在地面左侧在擂台
	}
}


// #define FD  150  //
// #define RD  150  //
// #define BD  150  //
// #define LD  180  //

#define FENCE_BACK 1
#define FENCE_LEFT 2
#define FENCE_FRONT 3
#define FENCE_RIGHT 4
#define FENCE_FRONT_LEFT 5
#define FENCE_FRONT_RIGHT 6
#define FENCE_BACK_RIGHT 7
#define FENCE_BACK_LEFT 8
#define FENCE_FRONT_BACK 9
#define FENCE_LEFT_RIGHT 10
#define FENCE_FRONT_LEFT_RIGHT 11
#define FENCE_FRONT_RIGHT_BACK 12
#define FENCE_FRONT_LEFT_BACK 13
#define FENCE_RIGHT_LEFT_BACK 14
#define FENCE_TITLED_FRONT_RIGHT 15
#define FENCE_TITLED_FRONT_LEFT 16
#define FENCE_TITLED_BACK_RIGHT 17
#define FENCE_TITLED_BACK_LEFT 18
#define FENCE_ERR 101
#define FD  250
#define RD  250
#define BD  170
#define LD  250
unsigned char Fence() { //在台下检测朝向
	AD1 = UP_ADC_GetValue(1); //底部前方红外光电
	AD2 = UP_ADC_GetValue(2); //底部右侧红外光电
	AD3 = UP_ADC_GetValue(3); //底部后方红外光电
	AD4 = UP_ADC_GetValue(4); //底部左侧红外光电
	AD5 = UP_ADC_GetValue(5); //前红外测距传感器
	AD6 = UP_ADC_GetValue(6); //右红外测距传感器
	AD7 = UP_ADC_GetValue(7); //后红外测距传感器
	AD8 = UP_ADC_GetValue(8); //左红外测距传感器

	/////////////////////////对擂台，一个测距检测到/////////////////////////
	if ((AD3 < 1000) && (AD2 > 1000) && (AD4 > 1000) && (AD5 > FD) && (AD6 < RD) &&
	        (AD7 < BD) && (AD8 < LD)) { //前测距和后红外
		return 1;       //在台下，后方对擂台
	}

	if ((AD4 < 1000) && (AD1 > 1000) && (AD3 > 1000) && (AD5 < FD) && (AD6 > RD) &&
	        (AD7 < BD) && (AD8 < LD)) { //右侧距左红外
		return 2;       //在台下，左侧对擂台
	}

	if ((AD1 < 1000) && (AD2 > 1000) && (AD4 > 1000) && (AD5 < FD) && (AD6 < RD) &&
	        (AD7 > BD) && (AD8 < LD)) { //后测距前红外
		return 3;       //在台下，前方对擂台
	}

	if ((AD2 < 1000) && (AD1 > 1000) && (AD3 > 1000) && (AD5 < FD) && (AD6 < RD) &&
	        (AD7 < BD) && (AD8 > LD)) { //左测距右红外
		return 4;       //在台下，右侧对擂台
	}
	/////////////////////////对围栏，两个相邻测距检测到/////////////////////////

	if ((AD2 > 1000) && (AD3 > 1000) && (AD5 > FD) && (AD6 < RD) && (AD7 < BD) &&
	        (AD8 > LD)) {
		return 5;       //在台下，前左检测到围栏
	}
	if ((AD3 > 1000) && (AD4 > 1000) && (AD5 > FD) && (AD6 > RD) && (AD7 < BD) &&
	        (AD8 < LD)) {
		return 6;       //在台下，前右检测到围栏
	}

	if ((AD1 > 1000) && (AD4 > 1000) && (AD5 < FD) && (AD6 > RD) && (AD7 > BD) &&
	        (AD8 < LD)) {
		return 7;       //在台下，后右检测到围栏
	}
	if ((AD1 > 1000) && (AD2 > 1000) && (AD5 < FD) && (AD6 < RD) && (AD7 > BD) &&
	        (AD8 > LD)) {
		return 8;       //在台下，后左检测到围栏
	}

	/////////////////////////台上有敌人，两个相对测距检测到/////////////////////////
	if ((AD5 > FD) && (AD6 < RD) && (AD7 > BD) && (AD8 < LD)) {
		return 9;       //在台下，前方或后方有台上敌人
	}

	if ((AD5 < FD) && (AD6 > RD) && (AD7 < BD) && (AD8 > LD)) {
		return 10;  //在台下，左侧或右侧由台上敌人
	}

	/////////////////////////三侧有障碍，三个测距检测到/////////////////////////
	if ((AD5 > FD) && (AD6 > RD) && (AD7 < BD) && (AD8 > LD)) {
		return 11;  //在台下，前方、左侧和右侧检测到围栏
	}
	if ((AD5 > FD) && (AD6 > RD) && (AD7 > BD) && (AD8 < LD)) {
		return 12;  //在台下，前方、右侧和后方检测到围栏
	}
	if ((AD5 > FD) && (AD6 < RD) && (AD7 > BD) && (AD8 > LD)) {
		return 13;  //在台下，前方、左侧和后方检测到围栏
	}
	if ((AD5 < FD) && (AD6 > RD) && (AD7 > BD) && (AD8 > LD)) {
		return 14;  //在台下，右侧、左侧和后方检测到围栏
	}


	/////////////////////////斜对擂台，两个红外光电检测到/////////////////////////
	if ((AD1 < 1000) && (AD2 < 1000) && (AD5 < FD) && (AD6 < RD)) {
		return 15;  //在台下，前方和右侧对擂台其他传感器没检测到
	}
	if ((AD1 < 1000) && (AD4 < 1000) && (AD5 < FD) && (AD8 < LD)) {
		return 16;  //在台下，前方和左侧对擂台其他传感器没检测到
	}
	if ((AD2 < 1000) && (AD3 < 1000) && (AD6 < FD) && (AD7 < RD)) {
		return 17;  //在台下，后方和右侧对擂台其他传感器没检测到
	}
	if ((AD3 < 1000) && (AD4 < 1000) && (AD7 < FD) && (AD8 < LD)) {
		return 18;  //在台下，后方和左侧对擂台其他传感器没检测到
	}

	//////////////
	else {
		return 101;//错误
	}

}

#define EDGE_NO_EDGE 0
#define EDGE_FRONT_LEFT 1
#define EDGE_FRONT_RIGHT 2
#define EDGE_BACK_RIGHT 3
#define EDGE_BACK_LEFT 4
#define EDGE_FRONT 5
#define EDGE_BACK 6
#define EDGE_LEFT 7
#define EDGE_RIGHT 8
#define EDGE_UNDERSTAGE 9
#define EDGE_ONSTAGE 10
#define EDGE_ERR 102
#define EDGE_UNDERSTAGE_FRONT 130
#define EDGE_ONSTAGE_BACK 130
unsigned char Edge() { //检测边缘
	AD9 = UP_ADC_GetValue(9);   //左前红外光电传感器
	AD10 = UP_ADC_GetValue(10); //右前红外光电传感器
	AD11 = UP_ADC_GetValue(11); //右后红外光电传感器
	AD12 = UP_ADC_GetValue(12); //左后红外光电传感器
	AD5 = UP_ADC_GetValue(5); //获取前方测距值
	AD7 = UP_ADC_GetValue(7); //获取后方测距值
	if ((AD9 < 1000) && (AD10 < 1000) && (AD11 < 1000) && (AD12 < 1000)) {
		return 0;       //没有检测到边缘
	} else if ((AD9 > 1000) && (AD10 < 1000) && (AD11 < 1000) && (AD12 < 1000)) {
		return 1;       //左前检测到边缘
	} else if ((AD9 < 1000) && (AD10 > 1000) && (AD11 < 1000) && (AD12 < 1000)) {
		return 2;       //右前检测到边缘
	} else if ((AD9 < 1000) && (AD10 < 1000) && (AD11 > 1000) && (AD12 < 1000)) {
		return 3;       //右后检测到边缘
	} else if ((AD9 < 1000) && (AD10 < 1000) && (AD11 < 1000) && (AD12 > 1000)) {
		return 4;       //左后检测到边缘
	} else if ((AD9 > 1000) && (AD10 > 1000) && (AD11 < 1000) && (AD12 < 1000)) {
		if (AD7 > EDGE_ONSTAGE_BACK) {
			return 10;
		} else {
			return 5;       //前方两个检测到边缘
		}
	} else if ((AD9 < 1000) && (AD10 < 1000) && (AD11 > 1000) && (AD12 > 1000)) {
		if (AD5 > EDGE_UNDERSTAGE_FRONT) {
			return 9;
		} else {
			return 6;       //后方两个检测到边缘
		}
	} else if ((AD9 > 1000) && (AD10 < 1000) && (AD11 < 1000) && (AD12 > 1000)) {
		return 7;       //左侧两个检测到边缘
	} else if ((AD9 < 1000) && (AD10 > 1000) && (AD11 > 1000) && (AD12 < 1000)) {
		return 8;       //右侧两个检测到边缘
	}
	// else if ((AD5 > 480)
	//            && (AD9 > 1000)
	//            && (AD10 > 1000)
	//            // && (AD11 > 1000)
	//            // && (AD12 > 1000)
	//           )	{
	// 	return 9;       //搁浅钱放在擂台下
	// } else if ((AD7 > 480)
	//            // && (AD9 > 1000)
	//            // && (AD10 > 1000)
	//            && (AD11 > 1000)
	//            && (AD12 > 1000)
	//           ) {
	// 	return 10;      //搁浅钱放在擂台上
	// }
	else {
		return 102;     //错误
	}
}

#define ENEMY_NO_ENEMY 0
#define ENEMY_FRONT 1
#define ENEMY_RIGHT 2
#define ENEMY_BACK 3
#define ENEMY_LEFT 4
#define ENEMY_ERROR 103
unsigned char Enemy() { //检测敌人
	AD1 = UP_ADC_GetValue(1); //前红外测距传感器
	AD2 = UP_ADC_GetValue(2); //右前红外光电传感器
	AD3 = UP_ADC_GetValue(3); //
	AD4 = UP_ADC_GetValue(4); //左中红外光电传感器
	AD5 = UP_ADC_GetValue(5); //左中红外光电传感器

	if ((AD1 > 100) && (AD2 > 100) && (AD3 > 100) && (AD4 > 100)) {
		return 0;  //无敌人
	}
	if ((AD1 < 100) && (AD2 > 100) && (AD3 > 100) && (AD4 > 100)) {
		if (AD5 > 1000) {
			return 11;  //前方是箱子
		} else {
			return 1;   //前方有棋子
		}
	}
	if ((AD1 > 100) && (AD2 < 100) && (AD3 > 100) && (AD4 > 100)) {
		return 2;   //右侧有敌人或棋子
	}
	if ((AD1 > 100) && (AD2 > 100) && (AD3 < 100) && (AD4 > 100)) {
		return 3;   //后方有敌人或棋子
	}
	if ((AD1 > 100) && (AD2 > 100) && (AD3 > 100) && (AD4 < 100)) {
		return 4;   //左侧有敌人或棋子
	} else {
		return 103;//错误
	}
}

int find_enemy = 0;
int find_enemy_count = 0;
void AttactEnemy() {
	int nEnemy = Enemy();

	UP_LCD_ClearLine(2);
	UP_LCD_ShowInt(2, 2, nEnemy);

	if (find_enemy) {

		if (find_enemy_count > 50) {
			find_enemy = 0;
			find_enemy_count = 0;
			UP_delay_ms(20);
			return;
		}

		if (nEnemy == ENEMY_FRONT || nEnemy == ENEMY_BACK || nEnemy == 11) {
			find_enemy = 0;
			find_enemy_count = 0;
			move(0, 0);
			UP_delay_ms(20);
		} else {
			find_enemy_count ++;
			UP_delay_ms(20);
			return;
		}
	}

	switch (nEnemy) {
	case ENEMY_NO_ENEMY:
		move(400, 400);
		UP_delay_ms(10);
		break;
	case ENEMY_FRONT:
		move(600, 600);
		UP_delay_ms(20);
		break;
	case ENEMY_RIGHT:
		move(400, -400);
		// UP_delay_ms(250);
		UP_delay_ms(20);
		find_enemy = 1;
		// move(400, 400);
		// UP_delay_ms(10);
		break;
	case ENEMY_BACK:
		move(-600, -600);
		UP_delay_ms(20);
		break;
	case ENEMY_LEFT:
		move(-400, 400);
		// UP_delay_ms(250);
		UP_delay_ms(20);
		find_enemy = 1;
		// move(400, 400);
		// UP_delay_ms(10);
		break;
	case 11:
		move(600, 600);
		UP_delay_ms(20);
		break;
	case ENEMY_ERROR:
		move(350, 350);
		UP_delay_ms(10);
		break;
		// default:
		// 	move(0, 0);
		// 	UP_delay_ms(10);
		// 	break;
	}
}

void OnStage() {
	na = 0;
	nb = 0;
	nEdge = Edge(); //检测边缘

	// nEdge = EDGE_NO_EDGE;

	UP_LCD_ClearLine(1);
	UP_LCD_ShowInt(1, 1, nEdge);

#ifndef DEBUG
	switch (nEdge) {
	case EDGE_NO_EDGE:
		// move(350, 350);
		// UP_delay_ms(10);
		AttactEnemy();
		break;
	case EDGE_FRONT_LEFT:
		move(-350, -350);
		UP_delay_ms(400);
		move(400, -400);
		UP_delay_ms(300);
		break;
	case EDGE_FRONT_RIGHT:
		move(-350, -350);
		UP_delay_ms(400);
		move(400, -400);
		UP_delay_ms(400);
		break;
	case EDGE_BACK_RIGHT:
		move(350, 350);
		UP_delay_ms(500);
		move(400, -400);
		UP_delay_ms(500);
		break;
	case EDGE_BACK_LEFT:
		move(350, 350);
		UP_delay_ms(500);
		move(400, -400);
		UP_delay_ms(500);
		break;
	case EDGE_FRONT:
		move(-350, -350);
		UP_delay_ms(400);
		move(400, -400);
		UP_delay_ms(300);
		break;
	case EDGE_BACK:
		move(350, 350);
		UP_delay_ms(300);
		break;
	case EDGE_LEFT:
		move(350, -350);
		UP_delay_ms(500);
		move(400, 400);
		UP_delay_ms(300);
		break;
	case EDGE_RIGHT:
		move(-350, 350);
		UP_delay_ms(500);
		move(400, 400);
		UP_delay_ms(300);
		break;
	case EDGE_UNDERSTAGE:
		nd++;
		if (nd > 15) {
			nd = 0;
			// move(0, 0);
			// UP_delay_ms(10);
			// move(-500, -500);
			// UP_delay_ms(200);
			// qding();
			// move(-500, -500);
			// UP_delay_ms(800);
			// zhong();
			// move(-500, -500);
			// UP_delay_ms(500);
			// move(500, -500);
			// UP_delay_ms(300);
			// move(0, 0);
			// UP_delay_ms(100);
			move(-500, 500);
			UP_CDS_SetAngle(5, 1000, 800);//左侧支地
			UP_CDS_SetAngle(7, 24, 800);
			UP_delay_ms(800);
			zhong();
			UP_delay_ms(600);
		} else {
			UP_delay_ms(20);
		}
		break;
	case EDGE_ONSTAGE:
		ne++;
		if (ne > 15) {
			ne = 0;
			// move(0, 0);
			// UP_delay_ms(10);
			// move(500, 500);
			// UP_delay_ms(200);
			// hding();
			// move(500, 500);
			// UP_delay_ms(800);
			// zhong();
			// move(500, 500);
			// UP_delay_ms(400);
			// move(0, 0);
			// UP_delay_ms(100);
			move(-500, 500);
			UP_CDS_SetAngle(5, 1000, 800);//左侧支地
			UP_CDS_SetAngle(7, 24, 800);
			UP_delay_ms(800);
			zhong();
			UP_delay_ms(600);
		} else {
			UP_delay_ms(20);
		}
		break;
	case EDGE_ERR:
		move(350, 350);
		UP_delay_ms(10);
		break;
	}
#else

#endif
}

void UnderStage() {

	nFence = Fence();   //检测边沿
	UP_LCD_ClearLine(1);
	UP_LCD_ShowInt(1, 1, nFence);

#ifndef DEBUG

	switch (nFence) {
	case FENCE_BACK:
		houshangtai();
		break;
	case FENCE_LEFT:
		move(0, 0);
		UP_delay_ms(200);
		while (1)
		{
			AD1 = UP_ADC_GetValue(1);
			AD6 = UP_ADC_GetValue(6);
			AD4 = UP_ADC_GetValue(4);
			if ((AD1 < 1000) && (AD6 < RD) && (AD4 > 1000))
			{
				UP_delay_ms(200);
				move(400, 400);
				UP_delay_ms(300);
				break;
			}
			else
			{
				move(-400, 400);
				UP_delay_ms(2);
			}
		} break;
	case FENCE_FRONT:
		qianshangtai();
		break;
	case FENCE_RIGHT:
		move(0, 0);
		UP_delay_ms(200);
		while (1)
		{
			AD1 = UP_ADC_GetValue(1);
			AD6 = UP_ADC_GetValue(6);
			AD4 = UP_ADC_GetValue(4);
			if ((AD1 < 1000) && (AD6 < 160) && (AD4 > 1000))
			{
				UP_delay_ms(200);
				move(400, 400);
				UP_delay_ms(300);
				break;
			}
			else
			{
				move(400, -400);
				UP_delay_ms(2);
			}
		}
		break;
	case FENCE_FRONT_LEFT:
		move(-400, -400);
		UP_delay_ms(400);
		break;
	case FENCE_FRONT_RIGHT:
		move(-400, -400);
		UP_delay_ms(400);
		break;
	case FENCE_BACK_RIGHT:
		move(400, 400);
		UP_delay_ms(400);
		break;
	case FENCE_BACK_LEFT:
		move(400, 400);
		UP_delay_ms(400);
		break;
	case FENCE_FRONT_BACK:
		move(500, -500);
		UP_delay_ms(300);
		move(400, 400);
		UP_delay_ms(400);
		break;
	case FENCE_LEFT_RIGHT:
		move(400, 400);
		UP_delay_ms(400);
		break;
	case FENCE_FRONT_LEFT_RIGHT:
		move(-400, -300);
		UP_delay_ms(500);
		move(-400, 400);
		UP_delay_ms(300);
		break;
	case FENCE_FRONT_RIGHT_BACK:
		move(300, 600);
		UP_delay_ms(400);
		break;
	case FENCE_FRONT_LEFT_BACK:
		move(600, 300);
		UP_delay_ms(400);
		break;
	case FENCE_RIGHT_LEFT_BACK:
		move(-400, 400);
		UP_delay_ms(200);
		move(400, 400);
		UP_delay_ms(300);
		break;
	case FENCE_TITLED_FRONT_RIGHT:
		move(0, 0);
		UP_delay_ms(200);
		while (1)
		{
			AD1 = UP_ADC_GetValue(1);
			AD6 = UP_ADC_GetValue(6);
			AD4 = UP_ADC_GetValue(4);
			if ((AD1 < 1000) && (AD6 < 160) && (AD4 > 1000))
			{
				UP_delay_ms(200);
				move(400, 400);
				UP_delay_ms(300);
				break;
			}
			else
			{
				move(360, -400);
				UP_delay_ms(2);
			}
		}
		break;
	case FENCE_TITLED_FRONT_LEFT: move(0, 0);
		UP_delay_ms(200);
		while (1)
		{
			AD1 = UP_ADC_GetValue(1);
			AD6 = UP_ADC_GetValue(6);
			AD4 = UP_ADC_GetValue(4);
			if ((AD1 < 1000) && (AD6 < 160) && (AD4 > 1000))
			{
				UP_delay_ms(200);
				move(400, 400);
				UP_delay_ms(300);
				break;
			}
			else
			{
				move(-400, 400);
				UP_delay_ms(2);
			}
		}
		break;
	case FENCE_TITLED_BACK_RIGHT:
		move(0, 0);
		UP_delay_ms(200);
		while (1)
		{
			AD1 = UP_ADC_GetValue(1);
			AD6 = UP_ADC_GetValue(6);
			AD4 = UP_ADC_GetValue(4);
			if ((AD1 < 1000) && (AD6 < 160) && (AD4 > 1000))
			{
				UP_delay_ms(200);
				move(400, 400);
				UP_delay_ms(300);
				break;
			}
			else
			{
				move(400, -500);
				UP_delay_ms(2);
			}
		}
		break;
	case FENCE_TITLED_BACK_LEFT:
		move(0, 0);
		UP_delay_ms(200);
		while (1)
		{
			AD1 = UP_ADC_GetValue(1);
			AD6 = UP_ADC_GetValue(6);
			AD4 = UP_ADC_GetValue(4);
			if ((AD1 < 1000) && (AD6 < 160) && (AD4 > 1000))
			{
				UP_delay_ms(200);
				move(400, 400);
				UP_delay_ms(300);
				break;
			}
			else
			{
				move(-420, 350);
				UP_delay_ms(2);
			}
		}
		break;
	case FENCE_ERR:
		UP_delay_ms(10);
		break;
	}
#endif
}

void OnStateCorner(int forward) {

#ifndef DEBUG
	switch (forward) {
	case STAGE_ONSTAGE_LEFT:
		na++;
		if (na == 350) {
			move(-500, 500);
			UP_CDS_SetAngle(5, 1000, 800);//左侧支地
			UP_CDS_SetAngle(7, 24, 800);
			UP_delay_ms(800);
			zhong();
			UP_delay_ms(600);
			na = 0;
		} else {
			UP_delay_ms(1);
		}
		break;
	case STAGE_ONSTAGE_RIGHT:
		na++;
		if (na == 350) {
			move(500, -500);
			UP_CDS_SetAngle(6, 24, 800);//右侧支地
			UP_CDS_SetAngle(8, 1000, 800);
			UP_delay_ms(800);
			zhong();
			UP_delay_ms(600);
			na = 0;
		} else {
			UP_delay_ms(1);
		}
		break;
	}
#endif
}

int main() {

	/*初始化系统*/
	UP_System_Init();
	UP_delay_ms(100);
	UP_CDS_SetMode(1, CDS_MOTOMODE);
	UP_CDS_SetMode(2, CDS_MOTOMODE);

	UP_CDS_SetMode(5, CDS_SEVMODE);
	UP_CDS_SetMode(6, CDS_SEVMODE);
	UP_CDS_SetMode(7, CDS_SEVMODE);
	UP_CDS_SetMode(8, CDS_SEVMODE);

	zhong();
	UP_delay_ms(1000);

	UP_LCD_ShowString(0, 0, "Ready");

	while (1) {
		if ((UP_ADC_GetValue(2) < 1000) || (UP_ADC_GetValue(4) < 1000)) {
			break;
		}
		UP_delay_ms(10);
	}

	UP_LCD_ShowString(0, 0, "Start");
	UP_delay_ms(1000);

#ifndef DEBUG
	// qianshangtai();
#endif

	UP_LCD_ClearScreen();

	while (1) {
		nStage = Stage();

		// nStage = STAGE_ONSTAGE;

		UP_LCD_ShowInt(0, 0, nStage);

		switch (nStage) {
		case STAGE_UNDERSTAGE:
			UnderStage();
			break;
		case STAGE_ONSTAGE:
			OnStage();
			break;
		case STAGE_ONSTAGE_LEFT:
			OnStateCorner(STAGE_ONSTAGE_LEFT);
			break;
		case STAGE_ONSTAGE_RIGHT:
			OnStateCorner(STAGE_ONSTAGE_RIGHT);
			break;
		}

#ifdef DEBUG
		if ( UP_Key_BACK()) {
			GenerateSystemReset();
		}
#endif

	}

	return 0;
}
